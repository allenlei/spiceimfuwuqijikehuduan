# SpiceIM服务器及客户端

#### 介绍
SpiceIM Server是一个基于NIO（V1）、AIO（V2）的客户、服务器端编程框架及IM基础服务能力开发平台，使用SpiceIM Server可以确保你快速和简单的开发出一个网络应用（包括推送服务，IM服务，业务插件服务）。SpiceIM Server相当简化和流程化了TCP的socket网络应用的编程开发过程。通过利用云端与客户端之间建立稳定、可靠的长连接来为开发者提供客户端应用与平台交互通道，开发者只需关注业务开发，无须关注底层网络交互。SpiceIM客户端包括Android及WebIM版本。将陆续开放Server端、Client端SDK及源码。同时征集ios app以及h5志愿开发人员共同完善SpiceIM其他接入端。欢迎开发人员加入：QQ群 218162018 QQ 25897555 微信 allenlei2010 另外欢迎提供公网服务器资源供平台演示环境部署。

 **PS：目前tio框架很火，笔者的android版本客户端同时也兼容接入tio。** 

Android版本客户端源码：https://gitee.com/allenlei/spiceim_client_android_version

#### 软件架构
软件架构说明
![输入图片说明](https://images.gitee.com/uploads/images/2020/0611/095110_7961f80a_409461.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0611/095127_2d091ea4_409461.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0611/095147_1ad2a096_409461.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0611/095207_71f39731_409461.png "屏幕截图.png")


征集ios app以及h5志愿开发人员共同完善SpiceIM其他接入端。欢迎开发人员加入：QQ群 218162018 QQ 25897555 微信 allenlei2010
另外欢迎提供公网服务器资源供平台演示环境部署。
欢迎star，欢迎捐赠！！！捐赠将用于平台演示环境资源租赁部署。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0730/085522_26a48405_409461.jpeg "微信图片_20200730085141.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0730/085539_3adccc5e_409461.jpeg "微信图片_20200730085150.jpg")